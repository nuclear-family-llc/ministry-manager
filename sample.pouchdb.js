/*
  Also be sure to check out embedding pouchdb-server as a way for
  an admin to administer the local DB files.
  Important: also look at the pouchdb auth options; should be able to implement
  this auth as the application auth itself.
*/

const PouchDB = require('pouchdb').defaults({ prefix: 'http://localhost:3001/'})
const pouchDbUpsert = require('pouchdb-upsert')

PouchDB.plugin(pouchDbUpsert)

const db = new PouchDB('test')

/**
 * @description Convenience method for a simple document merge on upsert.
 * @param {any} doc - The object to merge with the requested document.
 * @returns {Function} The merge function for the PouchDB upsert.
 * @example
 *  await db.upsert('some_id', merge({ some: 'document' }))
 */
const merge = function merge (doc) {
  /**
   * @param {any} prevDoc - The requsted document to merge against.
   * @returns {any} The merged document.
   */
  return function diffFun (prevDoc) {
    return Object.assign(prevDoc, doc)
  }
}

const main = async function main () {
  await db.upsert('ahuggins@newbz.net', merge({
    some: 'thing',
    goes: ['there'],
    and: {
      also: 'here'
    }
  }))
  console.log(await db.allDocs())
  //console.log(await db.get('ahuggins@newb.net'))
  console.log(await db.get('ahuggins@newbz.net'))

  const response = await db.upsert('ahuggins@newbie.net', merge({ some: 'stuff' }))

  console.log(response)
  console.log(await db.get(response.id))
}

main()
