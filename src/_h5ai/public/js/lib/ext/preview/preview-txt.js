const csjs = require('chordsheetjs');
const csjse = require('chordsheetjs-extras');
const lolight = require('lolight');
const MarkdownIt = require('markdown-it');
const {keys, dom} = require('../../util');
const allsettings = require('../../core/settings');
const preview = require('./preview');

const win = global.window;
const XHR = win.XMLHttpRequest;
const settings = Object.assign({
    enabled: false,
    styles: {}
}, allsettings['preview-txt']);
const preTpl = '<pre id="pv-content-txt"></pre>';
const divTpl = '<div id="pv-content-txt"></div>';

const updateGui = () => {
    const el = dom('#pv-content-txt')[0];
    if (!el) {
        return;
    }

    const container = dom('#pv-container')[0];
    el.style.height = container.offsetHeight + 'px';

    preview.setLabels([
        preview.item.label,
        preview.item.size + ' bytes'
    ]);
};

const requestTextContent = href => {
    return new Promise((resolve, reject) => {
        const xhr = new XHR();
        const callback = () => {
            if (xhr.readyState === XHR.DONE) {
                try {
                    resolve(xhr.responseText || '');
                } catch (err) {
                    reject(String(err));
                }
            }
        };

        xhr.open('GET', href, true);
        xhr.setRequestHeader('Cache-Control', 'max-age=600');
        xhr.onreadystatechange = callback;
        xhr.send();
    });
};

const load = item => {
    return requestTextContent(item.absHref)
        .catch(err => '[request failed] ' + err)
        .then(content => {
            const style = settings.styles[item.type];

            if (style === 1) {
                return dom(preTpl).text(content);
            } else if (style === 2) {
                const md = new MarkdownIt({
                    html: true,
                    linkify: true,
                    typographer: true
                });
                return dom(divTpl).html(md.render(content));
            } else if (style === 3) {
                const $code = dom('<code></code>').text(content);
                win.setTimeout(() => {
                    lolight.el($code[0]);
                }, content.length < 20000 ? 0 : 500);
                return dom(preTpl).app($code);
            } else if (style === 4) {
                const ChordSheetJS = csjs.default;
                const ChordSheetJSExtras = csjse.default;
                const parser = new ChordSheetJS.ChordProParser();
                const song = parser.parse(content.replace(/(\r\n)/gu,'\n'));
                const formatter = new ChordSheetJSExtras.HtmlTableExtFormatter();
                return dom(divTpl).html(`<div class="chordsheetjs">${formatter.format(song)}</div>`);
            }

            return dom(divTpl).text(content);
        });
};

const init = () => {
    if (settings.enabled) {
        preview.register(keys(settings.styles), load, updateGui);
    }
};

init();
